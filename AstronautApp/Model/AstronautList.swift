//
//  AstronautList.swift
//  AstronautApp
//
//  Created by DANA OBRIEN on 3/3/22.
//

import Foundation

struct AstronautList: Codable {
    let results: [Astronaut]
}
