//
//  Astronaut.swift
//  AstronautApp
//
//  Created by Robert King on 3/3/22.
//

import Foundation

struct Astronaut: Codable {
    let id: Int
    let name: String
    let nationality: String
    let profile_image: String
    let profile_image_thumbnail: String
    let date_of_birth: String
    let bio: String
}
