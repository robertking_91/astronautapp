//
//  AstronautPresenter.swift
//  AstronautApp
//
//  Created by Robert King on 3/3/22.
//

import Foundation

extension DispatchQueue {
    func executeMain(block: @escaping () -> Void) {
        DispatchQueue.main.async {
            block()
        }
    }
    
}

protocol AstronautView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func showDetails(astronaut: Astronaut)
}


class AstronautPresenter {
    private let astronautService: AstronautProvider
    weak private var astronautView: AstronautView?
    var astronautList: AstronautList?
    init(astronautService: AstronautProvider) {
        self.astronautService = astronautService
    }
    
    func attachView(_ attach: Bool, view: AstronautView?) {
        if attach {
            astronautView = nil
        } else {
            guard let view = view else {
                return
            }
            astronautView = view
        }
    }
    
    func sortAstronautsByName() {
        if let astroResults = astronautList?.results.sorted { astroOne, astroTwo in
            astroOne.name < astroTwo.name
        } {
            self.astronautList = AstronautList(results: astroResults)
            self.astronautView?.finishLoading()
        }
    }
    
    func getAstronauts() {
        self.astronautView?.startLoading()
        astronautService.getAstronauts { astroResult in
            switch astroResult {
            case .success(let astroList):
                self.astronautList = astroList
                self.astronautView?.finishLoading()
            case .failure(let apiError):
                break
            }
        }
    }
    
    func getAstronautByID(astroID: Int, completion: @escaping((Result<Astronaut,APIError>) -> Void)) {
        var astro: Astronaut?
        astronautService.getAstronautByID(astroID: astroID) { astroResult in
            switch astroResult {
            case .success(let astronaut):
                completion(.success(astronaut))
            case .failure(let apiError):
                completion(.failure(apiError))
            }
        }
    }
    
    func showAstronautDetails(astronaut: Astronaut) {
        DispatchQueue.main.async {
            self.astronautView?.showDetails(astronaut: astronaut)
        }
    }
    
}
