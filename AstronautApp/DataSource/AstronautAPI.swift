//
//  AstronautAPI.swift
//  AstronautApp
//
//  Created by Robert King on 3/3/22.
//

import Foundation

enum APIError: Error {
    case internalError
    case serverError
    case parsingError
}

protocol AstronautProvider {
    func getAstronauts(completion: @escaping((Result<AstronautList, APIError>) -> Void))
    func getAstronautByID(astroID: Int, completion: @escaping((Result<Astronaut, APIError>) -> Void))
}

class AstronautAPI: AstronautProvider {
    private let baseURL: String = "http://spacelaunchnow.me/api/3.5.0/astronaut/"
    private enum Endpoint {
        case getAstronauts
        case getAstronautByID(astroID: Int)
        var stringValue: String {
            switch self {
            case .getAstronauts:
                return ""
            case .getAstronautByID(let astroID):
                return "\(astroID)"
            }
        }
    }
    private enum Method: String {
        case GET
    }
    
    func getAstronauts(completion: @escaping ((Result<AstronautList, APIError>) -> Void)) {
        guard let astronautsRequest = request(endpoint: .getAstronauts, method: .GET) else {
            completion(.failure(.internalError))
            return
        }
        call(with: astronautsRequest) { (astronautResult: Result<AstronautList, APIError>) in
            switch astronautResult {
            case .success(let astroList):
                completion(.success(astroList))
            case .failure(let failure):
                completion(.failure(.parsingError))
            }
        }
    }
    
    func getAstronautByID(astroID: Int, completion: @escaping ((Result<Astronaut, APIError>) -> Void)) {
        guard let astronautIDRequest = request(endpoint: .getAstronautByID(astroID: astroID), method: .GET) else {
            completion(.failure(.internalError))
            return
        }
        call(with: astronautIDRequest) { (astroResponse: Result<Astronaut, APIError>) in
            switch astroResponse {
            case .success(let astronaut):
                completion(.success(astronaut))
            case .failure(let failure):
                completion(.failure(failure))
            }
        }
    }
    
    private func request(endpoint: Endpoint, method: Method) -> URLRequest? {
        let path = baseURL + endpoint.stringValue
        guard let url = URL(string: path) else {
            return nil
        }

        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = ["Content-Type": "application/json"]

        return request
    }

    private func call<T: Codable>(with request: URLRequest, completion: @escaping((Result<T, APIError>) -> Void)) {
        let dataTask = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                completion(.failure(.serverError))
                return
            }
            do {
                guard let data = data else {
                    completion(.failure(.serverError))
                    return
                }
                let object = try JSONDecoder().decode(T.self, from: data)
                completion(Result.success(object))
            } catch {
                completion(Result.failure(.parsingError))
            }
        }
        dataTask.resume()
    }
}


