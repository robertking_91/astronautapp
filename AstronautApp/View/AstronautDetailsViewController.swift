//
//  AstronautDetailViewController.swift
//  AstronautApp
//
//  Created by DANA OBRIEN on 3/6/22.
//

import UIKit

class AstronautDetailsViewController: UIViewController {
    @IBOutlet weak var profileImage: UIImageView! {
        didSet {
            profileImage.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var biographyTextView: UITextView!
    var astronautID: Int!
    var presenter: AstronautPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.view.showBlurLoader()
        }
        presenter.getAstronautByID(astroID: astronautID) { astroResult in
            switch astroResult {
            case .success(let astronaut):
                self.configure(with: astronaut)
            case .failure(let apiError):
                return
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func configure(with astronaut: Astronaut) {
        DispatchQueue.main.async {
            self.profileImage.loadImage(url: URL(string: astronaut.profile_image_thumbnail)!)
            self.nameLabel.text = "Name: " + astronaut.name
            self.nationalityLabel.text = "Nationality: " + astronaut.nationality
            self.dateOfBirthLabel.text = "Date of birth: " + astronaut.date_of_birth
            self.biographyTextView.text = astronaut.bio
            self.view.removeBlurLoader()
        }
    }
}
