//
//  ViewController.swift
//  AstronautApp
//
//  Created by Robert King on 3/3/22.
//

import UIKit

class AstronautListViewController: UIViewController {
    var presenter = AstronautPresenter(astronautService: AstronautAPI())
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    var spinnerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(false, view: self)
        presenter.getAstronauts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func sortAstronautsByName(_ sender: Any) {
        presenter.sortAstronautsByName()
    }
    
}

extension AstronautListViewController: AstronautView {
    func showDetails(astronaut: Astronaut) {
        if let detailsVC = mainStoryboard.instantiateViewController(withIdentifier: "AstronautDetailsViewController") as? AstronautDetailsViewController {
            self.navigationController?.pushViewController(detailsVC, animated: true)
            detailsVC.astronautID = astronaut.id
            detailsVC.presenter = presenter
        }
    }

    func startLoading() {
        DispatchQueue.main.async {
            self.view.showBlurLoader()
        }
    }
    
    func finishLoading() {
        DispatchQueue.main.async {
            self.view.removeBlurLoader()
            self.tableView.reloadData()
        }
    }
}

extension AstronautListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let astroCell = tableView.dequeueReusableCell(withIdentifier: "AstronautTableViewCell", for: indexPath) as? AstronautTableViewCell else {
            return UITableViewCell.init()
        }
        if let astronaut = presenter.astronautList?.results[indexPath.row] {
            astroCell.configure(astronaut: astronaut)
        }
        return astroCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.astronautList?.results.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let astroId = presenter.astronautList?.results[indexPath.row].id else {
            return
        }
        presenter.getAstronautByID(astroID: astroId) { astroResult in
            switch astroResult {
            case .success(let astronaut):
                self.presenter.showAstronautDetails(astronaut: astronaut)
            case .failure(let apiError):
                return
            }
        }
        
    }
}

final class AstronautTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.borderWidth = 3
    }
    
    func configure(astronaut: Astronaut) {
        nameLabel.text = "Name: " + astronaut.name
        nationalityLabel.text = "Nationality: " + astronaut.nationality
        profileImageView.loadImage(url: URL(string: astronaut.profile_image_thumbnail)!)
    }
}


